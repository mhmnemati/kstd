# kstd (KoLiBer Standard Library)

> kstd is a project for standardizing C language in `DSA`, `Network`, `File`, `Security`, etc for __cross platform__ usage (`android`, `ios`, `winapi32`, `linux`).

For more information about project please check [docs](docs/docs/README.md).